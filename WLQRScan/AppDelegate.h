//
//  AppDelegate.h
//  WLQRScan
//
//  Created by wangguoliang on 16/1/13.
//  Copyright © 2016年 wangguoliang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

